// In App.js in a new project

import * as React from 'react';
import { ImageBackground, View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import MainScreen from './src/screen/main'
import Student1Screen from './src/screen/Devin.js';
import Student2Screen from './src/screen/Sonam.js';
import Student3Screen from './src/screen/Rommy.js';

const Stack = createStackNavigator();

function App() {
  
  return (



    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={MainScreen} />
        <Stack.Screen name="Devin" component={Student1Screen} />
        <Stack.Screen name="Sonam" component={Student2Screen} />
        <Stack.Screen name="Rommy" component={Student3Screen} />

      </Stack.Navigator>

    </NavigationContainer>
  );

}
export default App;
