import React, { Component } from 'react';
import { ImageBackground, Alert, Image, View, Text, WebView, Button, StyleSheet } from 'react-native';


export default class Student1Screen extends Component {
  render(){
    return (
      <View style={styles.container}>
        <ImageBackground source={{uri:'https://i.pinimg.com/originals/28/d0/71/28d07105a480b923ebf544f90131e0d2.gif'}}
        style={styles.background}
        >
          <Text style ={styles.h1}>Devin Welch</Text>
          <Text style ={styles.h2}>Student ID: 6131305032</Text>
          <Image
            source={require('./img/dev.png')}
            style={{ marginTop:70, height: 250, width:250, borderRadius:250/2, overflow:"hidden" }}
            />
            <View style={styles.button}>
              <Button
                title="Say hi to Devin!"
                color="green"
                onPress={() => Alert.alert('You said Hi!')}
                />
            </View>
          </ImageBackground>
      </View>

      );
    }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',

  },

  h1: {
    color: 'white',
    fontSize: 40,
    marginTop:30,
  },

  h2: {
    color: 'white',
    fontSize: 20
  },

  button: {
    width:300,
    margin:30,
    width: "90%"
  },

  background: {
    width: '100%',
    height: '100%',
    alignItems:'center'
  }
});
