import * as React from 'react';
import { StyleSheet, View, Text , Image, ImageBackground, TouchableOpacity, Button} from 'react-native';

function MainScreen({ navigation }) {

  return (
    <View style={{ flex: 1}}>

      <TouchableOpacity style={{ flex: 1}} onPress={() => navigation.navigate('Devin')}>
        <ImageBackground
          source={{uri:'https://fall2017foundations.files.wordpress.com/2017/09/gif6.gif?w=454&h=340'}}
          style={styles.student}
        >
            <Text style={styles.h1}>Devin</Text>
        </ImageBackground>
    </TouchableOpacity>

    <TouchableOpacity style={{ flex: 1}} onPress={() => navigation.navigate('Sonam')}>
      <ImageBackground
        source={{uri:'https://media1.giphy.com/media/3oEdv5SY4LBHUBe2o8/giphy.gif?cid=790b7611e5f0ca87c14e77f73ad1eccb5857094969853636&rid=giphy.gif'}}
        style={styles.student}
      >
        <Text style={styles.h1}>Sonam</Text>
      </ImageBackground>
    </TouchableOpacity>

    <TouchableOpacity style={{ flex: 1}} onPress={() => navigation.navigate('Rommy')}>
      <ImageBackground
        source={{uri:'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/c6a5d27b-332a-4c1a-ab7c-e19e3d48a34c/dd2qixi-bebde108-7a6f-4694-86e3-0bcf5f15c5e5.gif?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcL2M2YTVkMjdiLTMzMmEtNGMxYS1hYjdjLWUxOWUzZDQ4YTM0Y1wvZGQycWl4aS1iZWJkZTEwOC03YTZmLTQ2OTQtODZlMy0wYmNmNWYxNWM1ZTUuZ2lmIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.899IpXyawQ14wDZ38Er8gu_uExTXxqZQQdpouJy5sq4'}}
        style={styles.student}
      >
          <Text style={styles.h1}>Rommy</Text>
      </ImageBackground>
    </TouchableOpacity>

    </View>
  );

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',

  },

  h1: {
    color: 'white',
    fontSize: 30,
    textShadowColor: 'black',
    textShadowRadius: 5

  },

  background: {
    width: '100%',
    height: '100%',
    alignItems:'center'

  },

  student: {
    flexDirection: 'row',
    height: '100%',
    width:'100%',
    overflow:'hidden',
    justifyContent: 'center',
    alignItems: 'center'

  }

});

export default MainScreen;
