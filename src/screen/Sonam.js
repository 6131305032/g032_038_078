import * as React from 'react';
import { StyleSheet, ImageBackground, Image, View, Text } from 'react-native';

function Student2Screen(){

  return (
   
    <View style={styles.container}>
      <ImageBackground style={ styles.Background} 
      source={{uri:'https://i.gifer.com/IIqH.gif'}}>

    <View style={{flex:1}}></View>
     <View style={ styles.ImageContainer}>
     <Image
       style={styles.Image}
       source={{uri:'https://i.pinimg.com/280x280_RS/64/ea/e3/64eae316b1dc465c31c474c3327f638c.jpg'}} />
      </View>
      <View style={styles.TextContainer}>
      <Text style={styles.h1}>Sonam Wangmo
      </Text>
      <Text style={styles.h2}> 6131305038</Text>
      </View>
     </ImageBackground>
     
    </View>
  
  );
       
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    

  },
 ImageContainer:{
  flex:3,
  justifyContent:"center", 
  alignItems:"center"
},
Image:{

    height: 250, 
    width:250, 
    borderRadius:250/2, 
    overflow:"hidden" 
},
TextContainer:{
  flex:1,
  justifyContent:"center", 
  alignItems:"center"
},
  h1: {
    color: "white",
    fontSize:30, 
    textDecorationStyle:"solid",
    textShadowColor: "pink", 
    textAlign: "left"
  },

  h2: {
    color: "white",
    fontSize:20, 
    textDecorationStyle:"solid",
    textShadowColor: "pink", 
    textAlign: "left"
  },

  Background: {
    width: '100%',
    height: '100%',
    alignItems:'center'
  }
});
export default Student2Screen;
